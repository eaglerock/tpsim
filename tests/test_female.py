import unittest
from tpsim import Female

class TestDefaultfemale(unittest.TestCase):
    def setUp(self):
        self.my_female = Female()

    def test_1_female_object_created(self):
        self.assertIsInstance(self.my_female, Female)

    def test_2_female_object_has_correct_gender(self):
        self.assertEqual(self.my_female.gender, 'female')

    def test_3_name_files_are_loaded_correctly(self):
        self.assertTrue(self.my_female.names['female'])
        self.assertFalse(self.my_female.names['male'])
        self.assertFalse(self.my_female.names['neutral'])

    def test_4_female_gets_random_name(self):
        self.assertIsNotNone(self.my_female.name)

    def tearDown(self):
        Female.purge_names()
