import unittest
from tpsim import Male

class TestDefaultMale(unittest.TestCase):
    def setUp(self):
        self.my_male = Male()

    def test_1_male_object_created(self):
        self.assertIsInstance(self.my_male, Male)

    def test_2_male_object_has_correct_gender(self):
        self.assertEqual(self.my_male.gender, 'male')

    def test_3_name_files_are_loaded_correctly(self):
        self.assertTrue(self.my_male.names['male'])
        self.assertFalse(self.my_male.names['female'])
        self.assertFalse(self.my_male.names['neutral'])

    def test_4_male_gets_random_name(self):
        self.assertIsNotNone(self.my_male.name)

    def tearDown(self):
        Male.purge_names()
