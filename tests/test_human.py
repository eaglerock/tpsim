import unittest
from tpsim import Human

class TestDefaultHuman(unittest.TestCase):
    def setUp(self):
        self.my_human = Human()

    def test_1_human_object_created(self):
        self.assertIsInstance(self.my_human, Human)

    def test_2_human_object_has_correct_gender(self):
        self.assertEqual(self.my_human.gender, 'neutral')

    def test_3_name_files_are_loaded_correctly(self):
        self.assertTrue(self.my_human.names['neutral'])
        self.assertFalse(self.my_human.names['male'])
        self.assertFalse(self.my_human.names['female'])

    def test_4_human_gets_random_name(self):
        self.assertIsNotNone(self.my_human.name)

    def test_5_replaced_rolls_are_counted_correctly(self):
        self.assertEqual(self.my_human.replaced_rolls, 0)
        self.my_human.replace_roll()
        self.my_human.replace_roll(2)
        self.assertEqual(self.my_human.replaced_rolls, 3)

    def test_6_number_ones_and_twos_are_counted_correctly(self):
        self.assertEqual(self.my_human.number_ones, 0)
        self.assertEqual(self.my_human.number_twos, 0)
        for _ in range(5):
            self.my_human.do_number(1)
        for _ in range(3):
            self.my_human.do_number(2)
        self.assertEqual(self.my_human.number_ones, 5)
        self.assertEqual(self.my_human.number_twos, 3)

    def test_7_do_number_function_works_correctly(self):
        self.assertFalse(self.my_human.do_number(1))
        self.assertTrue(self.my_human.do_number(2))
        self.assertRaises(Exception, self.my_human.do_number, None)
        self.assertRaises(Exception, self.my_human.do_number, 4)
        self.assertRaises(Exception, self.my_human.do_number, '2')

    def tearDown(self):
        Human.purge_names()

class TestCustomHuman(unittest.TestCase):
    def setUp(self):
        self.custom_human = Human(gender = 'male',
                                  name   = 'Peter',
                                  inches_per_use    = [14, 16],
                                  uses_per_number_2 = [4, 6])

    def test_1_human_object_created(self):
        self.assertIsInstance(self.custom_human, Human)

    def test_2_human_object_gets_correct_name_and_gender(self):
        self.assertEqual(self.custom_human.name, 'Peter')
        self.assertEqual(self.custom_human.gender, 'male')

    def test_3_name_files_are_loaded_correctly(self):
        self.assertFalse(self.custom_human.names['neutral'])
        self.assertFalse(self.custom_human.names['male'])
        self.assertFalse(self.custom_human.names['female'])

    def test_4_custom_kwargs_are_correct(self):
        self.assertEqual(self.custom_human.inches_per_use, [14, 16])
        self.assertEqual(self.custom_human.uses_per_number_2, [4, 6])

    def test_5_default_kwargs_are_correct(self):
        self.assertEqual(self.custom_human.uses_per_number_1, [1, 3])
        self.assertEqual(self.custom_human.number_1_is_tpre, False)

    def tearDown(self):
        Human.purge_names()

class TestGenderedHumans(unittest.TestCase):
    def test_1_invalid_genders_return_an_error(self):
        self.assertRaises(Exception, Human(), gender='alien')

if __name__ == '__main__':
    unittest.main()

