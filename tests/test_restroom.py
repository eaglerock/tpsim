import unittest
from tpsim import Restroom

class TestRestroom(unittest.TestCase):
    def setUp(self):
        Restroom.name_increment = 0   # Required to properly test incremental 
        self.my_restroom = Restroom()
        self.my_restroom_named = Restroom(name="Master Bathroom", tp_length = 10)
        self.my_restroom2 = Restroom(tp_fulllength=60, tp_length=0)

    def test_01_restroom_objects_created(self):
        self.assertIsInstance(self.my_restroom, Restroom)
        self.assertIsInstance(self.my_restroom_named, Restroom)
        self.assertIsInstance(self.my_restroom2, Restroom)

    def test_02_restrooms_are_named_correctly(self):
        self.assertEqual(self.my_restroom.name, 'Restroom 1')
        self.assertEqual(self.my_restroom_named.name, 'Master Bathroom')
        self.assertEqual(self.my_restroom2.name, 'Restroom 2')

    def test_03_default_tp_info_is_correct(self):
        self.assertEqual(self.my_restroom.tp_fulllength, 100)
        self.assertEqual(self.my_restroom.tp_length, 100 * 12)
        self.assertFalse(self.my_restroom.tp_is_empty)

    def test_04_custom_tp_info_is_correct(self):
        self.assertEqual(self.my_restroom_named.tp_length, 10)
        self.assertEqual(self.my_restroom2.tp_fulllength, 60)
        self.assertEqual(self.my_restroom2.tp_length, 0)
        self.assertTrue(self.my_restroom2.tp_is_empty)

    def test_05_bad_tp_lengths_raise_exceptions(self):
        self.assertRaises(Exception, Restroom, tp_fulllength=-50)
        self.assertRaises(Exception, Restroom, tp_length=1300)

    def test_06_negative_tp_lengths_raise_exceptions(self):
        self.assertRaises(Exception, Restroom, tp_length=-50)

    def test_07_using_tp_works_correctly(self):
        self.assertEqual(self.my_restroom.use_tp(25), 25)
        self.assertEqual(self.my_restroom.tp_length, 100 * 12 - 25)
        self.assertEqual(self.my_restroom_named.use_tp(25), 10)
        self.assertEqual(self.my_restroom_named.tp_length, 0)
        self.assertTrue(self.my_restroom_named.tp_is_empty)

    def test_08_refilling_tp_works_correctly(self):
        self.my_restroom2.refill_tp()
        self.assertEqual(self.my_restroom2.tp_length, 60 * 12)
        self.assertFalse(self.my_restroom2.tp_is_empty)

    def test_09_refill_counts_are_correct(self):
        for _ in range(5):
            self.my_restroom.refill_tp()
        self.assertEqual(self.my_restroom.refill_count, 5)

    def test_10_use_counts_are_correct(self):
        self.my_restroom2.tp_length = 100
        for _ in range(12):
            self.my_restroom2.use_tp(10)
        self.assertEqual(self.my_restroom2.use_count, 10)