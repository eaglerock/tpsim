from tpsim import Human

class Male(Human):
    """
    Male class - defines a biological male for the simulation

    This class provides male-centric defaults for the Human class, but still allows
    for full customization if desired.

    Instance Variables:
    gender - defaults to male, but can be changed for people who identify differently
    name   - name of the person, will be generated based on their gender, not sex
    """

    def __init__(self, gender='male', name=None, **kwargs):
        # Right now, just passes on sensible defaults to Human, but will do more later.
        super().__init__(gender, name, **kwargs)
