import random
import os

class Human:
    """
    Human class - define a human for the simulation

    This class defines a generic human for purposes of this simulation, allowing
    customization by a child class, by customization through kwargs, or with
    neutral defaults.

    Instance Variables:
    gender         - for purposes of generating a random name, defaults to neutral
    name           - name of human, will be generated at random if not defined
    replaced_rolls - tracks how many tp rolls the Human has replaced

    Keyword Arguments:
    number_1_is_tpre  - going #1 is a TPRE (TP-requiring event), defaults to false

    Keywork Arguments in list form:
    number_1s_per_day - range of #1's per day at location, defaults to [3, 5]
    number_2s_per_day - range of #2's per day at location, defaults to [0, 2]
    inches_per_use    - usage of TP per single use in inches, defaults to [16, 20]
    uses_per_number_1 - number of uses per #1 TPRE, defaults to [1, 3]
    uses_per_number_2 - number of uses per #2 TPRE, defaults to [3, 5]

    Instance Functions:
    do_number([1..2])   - increments the number, and returns whether event is a TPRE or not
    replace_roll(count) - replaces one roll by default, or replaces count rolls

    Class Functions:
    purge_names() - Clear all loaded name files (mostly for testing purposes)

    """

    # Class variables
    names = {     # tuples of random names per gender, loaded only when needed
        'male'    : (),
        'female'  : (),
        'neutral' : ()
    }

    def __init__(self, gender='neutral', name=None, **kwargs):
        if gender not in self.names.keys():
            raise Exception('ERROR: invalid gender!')
        else:
            self.gender = gender

        self.name               = name
        self.replaced_rolls     = 0
        self.number_ones        = 0
        self.number_twos        = 0
        self.number_1_is_tpre   = kwargs.pop('number_1_is_tpre',  False)
        self.number_1s_per_day  = kwargs.pop('number_1s_per_day', [3, 5])
        self.number_2s_per_day  = kwargs.pop('number_2s_per_day', [0, 2])
        self.inches_per_use     = kwargs.pop('inches_per_use',    [16, 20])
        self.uses_per_number_1  = kwargs.pop('uses_per_number_1', [1, 3])
        self.uses_per_number_2  = kwargs.pop('uses_per_number_2', [3, 5])

        if name is None:
            self._get_random_name()

    def _get_random_name(self):
        if not self.names[self.gender]:
            self._load_namefile(self.gender)

        self.name = random.choice(self.names[self.gender])

    def _load_namefile(self, gender):
        filename = 'names/' + gender + 'names'
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), filename)) as names:
            self.names[gender] = tuple(names.read().splitlines())

    def do_number(self, number):
        if number == 1:
            self.number_ones += 1
            return self.number_1_is_tpre
        elif number == 2:
            self.number_twos += 1
            return True
        elif not number:
            raise Exception('ERROR: number is required for do_number!')
        else:
            raise Exception('ERROR: invalid number for do_number!')

    def replace_roll(self, rolls = 1):
        self.replaced_rolls += rolls

    @classmethod
    def purge_names(cls):
        for i in cls.names:
            cls.names[i] = ()
