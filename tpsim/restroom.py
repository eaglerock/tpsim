import random

class Restroom:
    """
    Restroom class - define a restroom for the simulation
    
    This class defines a restroom that can be used for the simulation. Objects are
    intended to be uniquely named to allow one or more restrooms to be specified in a
    simulation. Individual uses of the room are tracked through this class.

    Instance Variables:
    name = unique name of restroom for final output

    Keyword Arguments:
    tp_fulllength = length of a full tp roll (60-200 feet is common)
    tp_length = current length of tp roll in inches (defaults to full roll * 12)
    """

    # Class variables
    name_increment = 0  # Used uniquely name restroom objects without a specified name

    def __init__(self, name=None, **kwargs):
        if 'tp_fulllength' in kwargs:
            if kwargs['tp_fulllength'] <= 0:
                raise Exception('ERROR: tp_fulllength cannot be <= 0!')

        if name is None:
            Restroom.name_increment += 1
            self.name = f"Restroom {Restroom.name_increment}"
        else:
            self.name = name

        self.use_count     = 0
        self.refill_count  = 0
        self.tp_fulllength = kwargs.pop('tp_fulllength', 100)
        self.tp_length = kwargs.pop('tp_length', self.tp_fulllength * 12)

        if self.tp_length > self.tp_fulllength * 12:
            raise Exception('ERROR: tp_length cannot be greater than tp_fulllength * 12!')
        elif self.tp_length < 0:
            raise Exception('ERROR: tp_length cannot be less than zero!')

    @property
    def tp_is_empty(self):
        return True if self.tp_length == 0 else False
    
    def use_tp(self, amount):
        # Returns the amount of tp actually given by the roll
        if self.tp_is_empty:
            return 0

        self.use_count += 1

        if amount >= self.tp_length:
            return_amt = self.tp_length
            self.tp_length = 0
            return return_amt
        else:
            self.tp_length -= amount
            return amount

    def refill_tp(self):
        self.refill_count += 1
        self.tp_length = self.tp_fulllength * 12
