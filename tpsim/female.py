from tpsim import Human

class Female(Human):
    """
    Female class - defines a biological female for the simulation

    This class provides female-centric defaults for the Human class, but still allows
    for full customization if desired. Also defines other biological needs specific to females.

    Instance Variables:
    gender - defaults to female, but can be changed for people who identify differently
    name   - name of the person, will be generated based on their gender, not sex
    """

    def __init__(self, gender='female', name=None, **kwargs):
        self.number_1_is_tpre = kwargs.pop('number_1_is_tpre', True)

        # Right now, just passes on sensible defaults to Human, but will do more later.
        super().__init__(gender, name, **kwargs)
