from tpsim import Human, Restroom

my_human = Human()
print("My human's name is {}.".format(my_human.name))

br1 = Restroom()
br2 = Restroom(name='Master Rest')
br3 = Restroom()

print(f"{br1.name}, {br2.name}, {br3.name}")